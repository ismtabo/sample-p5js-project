class Soil {
  constructor(world, sprite, width, x, y) {
    this.world = world;
    this.width = width;
    this.x = x;
    this.y = y;
    this.world.objects.push(this);
    this.parts = [];
    let numberOfParts = Math.ceil(width / sprite.width);
    for (let i = 0; i < numberOfParts; i++) {
        this.parts.push(new SoilPart(this, sprite, sprite.width * i + x, y));
    }
  }

  update() {
    this.parts.forEach(part => {
        part.update();
    })
  }

  draw() {
    this.parts.forEach(part => {
        part.draw();
    })
  }
}

class SoilPart {
  constructor(soil, sprite, x, y) {
    this.soil = soil;
    this.sprite = sprite;
    this.x = x;
    this.y = y;
  }

  update() {
    this.x -= this.soil.world.x;
    if(this.x + this.sprite.width < this.soil.x) {
        this.x = this.soil.width;
    }
  }

  draw() {
    image(this.sprite, this.x, this.y);
  }
}
