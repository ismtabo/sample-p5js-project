class Player {
  constructor(world, states, x, y) {
    this.state = null;
    this.states = states;
    this.x = x;
    this.y = y;
    this.world = world;
    this.world.objects.push(this);
  }

  update() {
    if (this.state) {
      this.state.update(this);
    }
  }

  draw() {
    if (this.state) {
      this.state.draw(this);
    }
  }

  setState(state) {
    if (this.state) {
      this.state.end(this);
    }
    this.state = state;
    this.state.init(this);
  }

  getState() {
    return this.state;
  }

  jump() {
    this.setState(this.states.jump);
  }

  rest() {
    this.setState(this.states.rest);
  }

  run() {
    this.setState(this.states.run);
  }
}
