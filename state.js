class State {
  constructor(frames, next) {
    this.frames = frames;
    this.length = frames.length;
    this.next = next;
  }

  init() {}

  end() {}

  draw(character) {
    this.show(this.frames[Math.round(frameCount) % this.length], character.x, character.y);
  }

  show(frame, x, y) {
    image(frame, x, y);
  }

  update(character) {}
}

class JumpState extends State {
  constructor(frames, next, dy) {
    super(frames, next);
    this.dy = dy;
    this.jumpLength = Math.ceil(this.length / 2);
    this.jumpFrame = 0;
  }

  init(character) {
    this.initY = character.y;
    this.character = character;
    this.jumpFrame = 0;
  }

  end(character) {
    character.y = this.initY;
    this.character = null;
  }

  update(character) {
    if (this.jumpFrame < this.jumpLength) {
      character.y = map(this.jumpFrame, 0, this.jumpLength, this.initY - this.dy, this.initY);
    } else {
      character.y = map(this.jumpFrame, this.jumpLength, this.length, this.initY - this.dy, this.initY);
    }

    this.jumpFrame += 1;
    if (this.jumpFrame > this.length) {
      this.character.setState(this.next);
    }
  }
}

class RunState extends State {
  constructor(frames, next, dx) {
    super(frames, next);
    this.dx = dx;
  }

  end(character) {
    character.world.x = 0;
  }

  update(character) {
    character.world.x = this.dx;
    character.world.x %= character.world.width;
  }
}
