var world;
var rambo;
var states;
var backgroundImage;
var soilImage;
var soil = [];

function preload() {
  backgroundImage = loadImage("assests/img/background.png");
  soilImage = loadImage("assests/img/soil.png");

  let restImages = [];
  let restImage = loadImage("assests/img/rambo/gs1.gif");
  restImages.push(restImage);

  let jumpImages = [];
  for (let i = 0; i < 7; i++) {
    let jumpImage = loadImage("assests/img/rambo/gj" + i + ".gif");
    jumpImages.push(jumpImage);
  }

  let runImages = [];
  for (let i = 0; i < 5; i++) {
    let runImage = loadImage("assests/img/rambo/gw" + i + ".gif");
    runImages.push(runImage);
  }

  let rest = new State(restImages, null);
  let jump = new JumpState(jumpImages, rest, 50);
  let run = new RunState(runImages, null, 10);

  states = {rest, jump, run};
}

function setup() {
  createCanvas(900, 400);
  soilImage = soilImage.get(0, 16, 64, 16);
  soilImage.resize(soilImage.width * 2, soilImage.height * 2);
  world = new World(900);
  soil = new Soil(world, soilImage, width, 0, height - soilImage.height);
  rambo = new Player(world, states, 0, height - 144);
  rambo.rest();
  frameRate(30);
}

function draw() {
  background(backgroundImage);
  world.update();
  world.draw();
  console.log(world.x);
}

function keyTyped() {
  switch (key) {
    case "w":
      rambo.jump();
      break;
  }
}

function keyPressed() {
  switch (key) {
    case "d":
      rambo.run();
      break;
  }
}

function keyReleased() {
  switch (key) {
    case "d":
      rambo.rest();
      break;
  }
}
