class World {
  constructor(width) {
    this.x = 0;
    this.width = width;
    this.objects = [];
  }

  update() {
    this.objects.forEach(object => {
      object.update();
    });
  }

  draw() {
    this.objects.forEach(object => {
      object.draw();
    });
  }
}
